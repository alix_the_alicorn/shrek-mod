package jazon.shrek.block;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import jazon.shrek.helper.Helper;
import jazon.shrek.lib.Names;
import jazon.shrek.proxy.register.RegisterItem;
import net.minecraft.block.Block;
import net.minecraft.block.BlockCrops;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.EnumPlantType;

import static net.minecraftforge.common.EnumPlantType.*;
import static net.minecraftforge.common.EnumPlantType.Plains;

public class BlockOnionPlant extends BlockCrops
{
    public BlockOnionPlant() {
        super();
        setBlockTextureName(Names.onionPlantUnlocalizedName);
        setBlockName(Names.onionPlantUnlocalizedName);
    }

    @SideOnly(Side.CLIENT)
    private IIcon[] field_149868_a;

    /**
     * Gets the block's texture. Args: side, meta
     */
    @SideOnly(Side.CLIENT)
    public IIcon getIcon(int p_149691_1_, int p_149691_2_)
    {
        if (p_149691_2_ < 7)
        {
            if (p_149691_2_ == 6)
            {
                p_149691_2_ = 5;
            }

            return this.field_149868_a[p_149691_2_ >> 1];
        }
        else
        {
            return this.field_149868_a[3];
        }
    }

    protected Item func_149866_i()
    {
        return RegisterItem.onion;
    }

    protected Item func_149865_P()
    {
        return RegisterItem.onion;
    }

    @SideOnly(Side.CLIENT)
    public void registerBlockIcons(IIconRegister p_149651_1_)
    {
        this.field_149868_a = new IIcon[4];

        for (int i = 0; i < this.field_149868_a.length; ++i)
        {
            if(i != 3) {
                this.field_149868_a[i] = p_149651_1_.registerIcon("potatoes_stage_" + i);
            } else {
                this.field_149868_a[i] = p_149651_1_.registerIcon(Helper.MOD_ID + ":" + "onions_stage_" + i);
            }
        }
    }

    @Override
    public EnumPlantType getPlantType(IBlockAccess world, int x, int y, int z)
    {
        return Crop;
    }
}