package jazon.shrek.lib;

import jazon.shrek.helper.Helper;

public class Names {
    //Items
    public static final String spawnEggName = "spawn_egg";
    public static final String spawnEggUnlocalizedName = unlocalize(spawnEggName);
    public static final String onionName = "onion";
    public static final String onionUnlocalizedName = unlocalize(onionName);
    public static final String onionOnAStickName = "onion_on_a_stick";
    public static final String onionOnAStickUnlocalizedName = unlocalize(onionOnAStickName);
    public static final String shrekKillerName = "shrekKiller";
    public static final String shrekKillerUnlocalizedName = unlocalize(shrekKillerName);
    public static final String parfaitName = "parfait";
    public static final String parfaitUnlocalizedName = unlocalize(parfaitName);
    public static final String genericName = "generic";
    public static final String genericUnlocalizedName = unlocalize(genericName);

    //Blocks
    public static final String onionPlantName = "onionPlant";
    public static final String onionPlantUnlocalizedName = unlocalize(onionPlantName);

    //Helper methods
    private static String unlocalize(String name) {
        return Helper.MOD_ID + ":" + name;
    }
}
