package jazon.shrek.lib;

public class OreDictNames {
    private static final String crop = "crop";
    private static String getCrop(String type) {
        return crop + type;
    }

    public static final String cropOnion = getCrop("Onion");
}
