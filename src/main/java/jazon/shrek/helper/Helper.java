package jazon.shrek.helper;

import net.minecraftforge.common.config.Configuration;

public class Helper {
    public static final String MOD_ID = "shrek";
    public static final String MOD_NAME = "Shrek";
    public static final String MOD_VERSION = "@version@ [build # @build@]";
    public static final String SERVER_PROXY = "jazon.shrek.proxy.ServerProxy";
    public static final String CLIENT_PROXY = "jazon.shrek.proxy.ClientProxy";
}
