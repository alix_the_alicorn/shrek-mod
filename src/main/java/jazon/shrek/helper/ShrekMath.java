package jazon.shrek.helper;

import jazon.shrek.lib.Integers;

public class ShrekMath {
    public static final int getTicksForSeconds(int seconds) {
        return seconds * 20;
    }
    public static final float getSecondsForTicks(int ticks) {
        return ticks / 20;
    }
    public static final float getPixels(float amountOfPixelsToReturn) {
        return Integers.pixel * amountOfPixelsToReturn;
    }
}
