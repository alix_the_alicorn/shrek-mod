package jazon.shrek.helper;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import net.minecraftforge.common.util.FakePlayer;

public class WorldHelper {
    public static void spawnStackInWorld(World w, double x, double y, double z, ItemStack s) {
        if(w != null && s != null) {
            EntityItem i = new EntityItem(w, x, y, z, s);
            if (!w.isRemote) {
                w.spawnEntityInWorld(i);
            }
        }
        if(w == null) {
            System.out.println("A mod tried to spawn an item in the world using the shrek mod's \"WorldHelper.spawnStackInWorld(World w, double x, double y, double z, ItemStack s)\". Is client world?: " + w.isRemote);
            System.out.println("But an error has happened and the World object passed into the method was null. Is client world?: " + w.isRemote);
        }
        if(s == null) {
            System.out.println("A mod tried to spawn an item in the world using the shrek mod's \"WorldHelper.spawnStackInWorld(World w, double x, double y, double z, ItemStack s)\". Is client world?: " + w.isRemote);
            System.out.println("But an error has happened and the World object passed into the method was null. Is client world?: " + w.isRemote);
        }
    }

    public static void spawnStackInWorldAtPlayer(EntityPlayer p, ItemStack s) {
        World w = p.worldObj;
        if(w != null && s != null) {
            EntityItem i = new EntityItem(w, p.posX, p.posY, p.posZ, s);
            if (!w.isRemote) {
                w.spawnEntityInWorld(i);
                if(!(p instanceof FakePlayer)) {
                    i.onCollideWithPlayer(p);
                }
            }
        }
        if(w == null) {
            System.out.println("A mod tried to spawn an item in the world using the shrek mod's \"WorldHelper.spawnStackInWorldAtPlayer(World w, EntityPlayer p, ItemStack s)\". Is client world?: " + w.isRemote);
            System.out.println("But an error has happened and the World object passed into the method was null. Is client world?: " + w.isRemote);
        }
        if(s == null) {
            System.out.println("A mod tried to spawn an item in the world using the shrek mod's \"WorldHelper.spawnStackInWorldAtPlayer(World w, EntityPlayer p, ItemStack s)\". Is client world?: " + w.isRemote);
            System.out.println("But an error has happened and the World object passed into the method was null. Is client world?: " + w.isRemote);
        }
    }
}