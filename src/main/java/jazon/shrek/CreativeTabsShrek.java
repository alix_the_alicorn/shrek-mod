package jazon.shrek;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import jazon.shrek.proxy.register.RegisterItem;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class CreativeTabsShrek {
    public static CreativeTabs tabShrekMain = new CreativeTabs("shrek:main") {
        @Override
        @SideOnly(Side.CLIENT)
        public Item getTabIconItem() {
            return RegisterItem.onion;
        }
    };
    public static CreativeTabs tabShrekCreative = new CreativeTabs("shrek:creative") {
        @Override
        @SideOnly(Side.CLIENT)
        public Item getTabIconItem() {
            return RegisterItem.spawnEgg;
        }
    };
}