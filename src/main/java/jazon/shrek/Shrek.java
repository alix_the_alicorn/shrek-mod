package jazon.shrek;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;

import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import jazon.shrek.helper.Helper;
import jazon.shrek.proxy.IProxy;

@Mod(modid=Helper.MOD_ID, name=Helper.MOD_NAME, version=Helper.MOD_VERSION)
public class Shrek {
    //TODO Add donkey
    //TODO Add Fiona
    //TODO Add dragon

    //TODO Add Shrek house
    //TODO Add Shrek porta toilet

    @Mod.Instance(Helper.MOD_ID)
    public static Shrek instance;

    @SidedProxy(clientSide=Helper.CLIENT_PROXY, serverSide=Helper.SERVER_PROXY )
    public static IProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        proxy.commonPreInitialize(event);
        proxy.clientPreInitialize(event);
        proxy.serverPreInitialize(event);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.commonInitialize(event);
        proxy.clientInitialize(event);
        proxy.serverInitialize(event);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.commonPostInitialize(event);
        proxy.clientPostInitialize(event);
        proxy.serverPostInitialize(event);
    }
}