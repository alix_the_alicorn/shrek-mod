package jazon.shrek.client.render.entity.living;

import jazon.shrek.entity.living.EntityShrek;
import jazon.shrek.helper.Helper;
import jazon.shrek.helper.ShrekMath;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.AdvancedModelLoader;
import net.minecraftforge.client.model.IModelCustom;
import org.lwjgl.opengl.GL11;

public class RenderEntityShrek extends Render {

    public static final ResourceLocation[] textureImposter = {
            new ResourceLocation(Helper.MOD_ID, "model/shrek/imposter/Imposter_Shrek.png")
    };
    public static final ResourceLocation[] textureNormal = {
            new ResourceLocation(Helper.MOD_ID, "model/shrek/normal/Shrek_Body.png"),
            new ResourceLocation(Helper.MOD_ID, "model/shrek/normal/Shrek_Head_Legs.png")
    };
    public static final ResourceLocation[] textureKnight = {
            new ResourceLocation(Helper.MOD_ID, "model/shrek/knight/Shrek_Knight_Armor.png"),
            new ResourceLocation(Helper.MOD_ID, "model/shrek/knight/Shrek_Knight_Body.png"),
            new ResourceLocation(Helper.MOD_ID, "model/shrek/knight/Shrek_Knight_Head.png")
    };
    public static final ResourceLocation[] texturePirate = {
            new ResourceLocation(Helper.MOD_ID, "model/shrek/pirate/Shrek_Pirate_Body.png"),
            new ResourceLocation(Helper.MOD_ID, "model/shrek/pirate/Shrek_Pirate_Head.png")
    };
    public static final ResourceLocation[] textureRegal = {
            new ResourceLocation(Helper.MOD_ID, "model/shrek/regal/Shrek_regal_body.png"),
            new ResourceLocation(Helper.MOD_ID, "model/shrek/regal/Shrek_Regal_Head.png")
    };
    public static final ResourceLocation[] textureSwim = {
            new ResourceLocation(Helper.MOD_ID, "model/shrek/swim/Shrink_Swim_Body.png"),
            new ResourceLocation(Helper.MOD_ID, "model/shrek/swim/Shrek_swim_Head.png")
    };

    public RenderEntityShrek() {
    }

    @Override
    public void doRender(Entity entity, double x, double y, double z, float pitch, float yaw) {
        EntityShrek e = (EntityShrek)entity;
        final IModelCustom model = AdvancedModelLoader.loadModel(new ResourceLocation(Helper.MOD_ID, "model/shrek/" + e.getShrekType().getFolder() + "/model.obj"));

        switch(e.getShrekType()) {
            case IMPOSTER:
                renderAll(entity, textureImposter[0], model, x, y, z);
                break;
            case NORMAL:
                renderPart(entity, textureNormal[0], model, "Mesh_0105", x, y, z);
                renderPart(entity, textureNormal[0], model, "Mesh_0106", x, y, z);

                renderPart(entity, textureNormal[1], model, "Mesh_0103", x, y, z);
                renderPart(entity, textureNormal[1], model, "Mesh_0104", x, y, z);
                break;
            case KNIGHT:
                renderPart(entity, textureKnight[0], model, "Mesh_0190", x, y, z);
                renderPart(entity, textureKnight[1], model, "Mesh_0191", x, y, z);
                renderPart(entity, textureKnight[1], model, "Mesh_0192", x, y, z);
                renderPart(entity, textureKnight[2], model, "Mesh_0193", x, y, z);
                break;
            case PIRATE:
                renderPart(entity, texturePirate[1], model, "Mesh_0203", x, y, z);
                renderPart(entity, texturePirate[0], model, "Mesh_0204", x, y, z);
                renderPart(entity, texturePirate[0], model, "Mesh_0205", x, y, z);
                break;
            case REGAL:
                renderPart(entity, textureRegal[0], model, "Mesh_0058", x, y, z);
                renderPart(entity, textureRegal[0], model, "Mesh_0059", x, y, z);
                renderPart(entity, textureRegal[1], model, "Mesh_0060", x, y, z);
                renderPart(entity, textureRegal[1], model, "Mesh_0061", x, y, z);
                break;
            case SWIM:
                renderPart(entity, textureSwim[1], model, "Mesh_0123", x, y, z);
                renderPart(entity, textureSwim[0], model, "Mesh_0124", x, y, z);
                renderPart(entity, textureSwim[0], model, "Mesh_0125", x, y, z);
                break;
        }
    }

    @Override
    protected ResourceLocation getEntityTexture(Entity var1) {
        return null;
    }

    private void renderPart(Entity e, ResourceLocation texture, IModelCustom modelCustom, String part, double x, double y, double z) {
        bindTexture(texture);

        Tessellator.instance.setColorOpaque_F(1, 1, 1);

        GL11.glPushMatrix();

        GL11.glTranslatef((float)x, (float)y + 0.3f, (float)z + ShrekMath.getPixels(4));
        GL11.glRotatef(360.0f - ((e.prevRotationYaw + e.rotationYaw) / 2), 0.0F, 1.0F, 0.0F);
        //GL11.glRotatef(entity.prevRotationPitch + (entity.rotationPitch - entity.prevRotationPitch) * yaw, 0.0F, 0.0F, 1.0F);
        GL11.glScalef(.02f, .02f, .02f);

        modelCustom.renderPart(part);

        GL11.glPopMatrix();
    }

    private void renderAll(Entity e, ResourceLocation texture, IModelCustom modelCustom, double x, double y, double z) {
        bindTexture(texture);

        Tessellator.instance.setColorOpaque_F(1, 1, 1);

        GL11.glPushMatrix();

        GL11.glTranslatef((float)x, (float)y + 0.3f, (float)z + ShrekMath.getPixels(4));
        GL11.glRotatef(360.0f - ((e.prevRotationYaw + e.rotationYaw) / 2), 0.0F, 1.0F, 0.0F);
        //GL11.glRotatef(entity.prevRotationPitch + (entity.rotationPitch - entity.prevRotationPitch) * yaw, 0.0F, 0.0F, 1.0F);
        GL11.glScalef(.02f, .02f, .02f);

        modelCustom.renderAll();

        GL11.glPopMatrix();
    }
}
