package jazon.shrek.client.model.entity.living;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.model.ModelQuadruped;
import net.minecraft.client.model.ModelRenderer;

@SideOnly(Side.CLIENT)
public class ModelEntityShrekDonkey extends ModelQuadruped
{
    public ModelEntityShrekDonkey()
    {
        this(12, 0.0F);
    }

    public ModelEntityShrekDonkey(int uno, float p_i1151_1_)
    {
        super(uno, p_i1151_1_);
        head.setTextureOffset(16, 16).addBox(-2.0F, 0.0F, -9.0F, 4, 3, 1, p_i1151_1_);
    }
}