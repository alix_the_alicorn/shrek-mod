package jazon.shrek.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import jazon.shrek.CreativeTabsShrek;
import jazon.shrek.helper.Helper;
import jazon.shrek.helper.WorldHelper;
import jazon.shrek.lib.Names;
import jazon.shrek.proxy.register.RegisterBlock;
import jazon.shrek.proxy.register.RegisterItem;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.FoodStats;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.common.EnumPlantType;
import net.minecraftforge.common.IPlantable;
import net.minecraftforge.common.util.ForgeDirection;

import java.util.List;

public class ItemParfait extends Item {
    public static final String[] names = new String[] { "6layers", "5layers", "4layers", "3layers", "2layers", "1layer" };
    public static final int subtypesAmount = names.length;

    public ItemParfait() {
        super();
        setUnlocalizedName(Names.parfaitUnlocalizedName);
        setTextureName(Names.parfaitUnlocalizedName);
        setHasSubtypes(true);
        setCreativeTab(CreativeTabsShrek.tabShrekMain);
        setMaxStackSize(1);
    }

    @SideOnly(Side.CLIENT)
    private IIcon[] icons;

    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister par1IconRegister) {
        icons = new IIcon[subtypesAmount];
        for(int i = (subtypesAmount - 1); i >= 0; --i) {
            icons[i] = par1IconRegister.registerIcon(Helper.MOD_ID + ":" + Names.parfaitName + "/" + Names.parfaitName + i);
        }
    }

    public IIcon getIconFromDamage(int i) {
        return icons[i];
    }

    @SideOnly(Side.CLIENT)
    public void getSubItems(Item par1, CreativeTabs par2CreativeTabs, List par3List) {
        for (int x = 0; x < subtypesAmount; x++) {
            par3List.add(new ItemStack(this, 1, x));
        }
    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer player, List text, boolean whatDoesThisRandomBooleanDoQuestionMark)
    {
        text.add("\"Parfait's gotta be the most delicious thing on the whole damn planet!\" - Donkey");
        text.add("");
        if(6 - stack.getItemDamage() != 1) {
            text.add("This " + EnumChatFormatting.RED + "parfait" + EnumChatFormatting.RESET + EnumChatFormatting.GRAY + " has " + (6 - stack.getItemDamage()) + EnumChatFormatting.AQUA + " bites " + EnumChatFormatting.RESET + EnumChatFormatting.GRAY + "left.");
        } else {
            text.add("This " + EnumChatFormatting.RED + "parfait" + EnumChatFormatting.RESET + EnumChatFormatting.GRAY + " has " + (6 - stack.getItemDamage()) + EnumChatFormatting.AQUA + " bites " + EnumChatFormatting.RESET + EnumChatFormatting.GRAY + "left.");
        }
        super.addInformation(stack, player, text, whatDoesThisRandomBooleanDoQuestionMark);
    }

    @Override
    public ItemStack onEaten(ItemStack stack, World world, EntityPlayer player) {
        if(!world.isRemote) {
            FoodStats f = player.getFoodStats();
            f.setFoodLevel(f.getFoodLevel() + 2);
            if(f.getFoodLevel() > 20) {
                f.setFoodLevel(20);
            }
            if(stack.getItemDamage() + 1 <= 5) {
                stack.setItemDamage(stack.getItemDamage() + 1);
            } else {
                stack.stackSize = 0;
                WorldHelper.spawnStackInWorldAtPlayer(player, new ItemStack(RegisterItem.generic, 1, 0));
            }
            player.inventory.setInventorySlotContents(player.inventory.currentItem, stack);
        }
        return stack;
    }


    @Override
    public int getMaxItemUseDuration(ItemStack stack) {
        return 32;
    }

    @Override
    public EnumAction getItemUseAction(ItemStack stack) {
        return EnumAction.eat;
    }

    @Override
    public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player) {
        if(!player.isSneaking()) {
            player.setItemInUse(stack, this.getMaxItemUseDuration(stack));
        }
        return stack;
    }

    @Override
    public boolean getShareTag() {
        return true;
    }

    public boolean onItemUse(ItemStack p_77648_1_, EntityPlayer p_77648_2_, World p_77648_3_, int p_77648_4_, int p_77648_5_, int p_77648_6_, int p_77648_7_, float p_77648_8_, float p_77648_9_, float p_77648_10_)
    {
        return super.onItemUse(p_77648_1_, p_77648_2_, p_77648_3_, p_77648_4_, p_77648_5_, p_77648_6_, p_77648_7_, p_77648_8_, p_77648_9_, p_77648_10_);
    }
}