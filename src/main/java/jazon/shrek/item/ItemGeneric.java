package jazon.shrek.item;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import jazon.shrek.CreativeTabsShrek;
import jazon.shrek.lib.Names;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

import java.util.List;

public class ItemGeneric extends Item {
    public static final String[] names = new String[] { "parfaitCup" };
    public static final int subtypesAmount = names.length;

    public ItemGeneric() {
        super();
        setUnlocalizedName(Names.genericUnlocalizedName);
        setTextureName(Names.genericUnlocalizedName);
        setHasSubtypes(true);
        setCreativeTab(CreativeTabsShrek.tabShrekMain);
    }

    @SideOnly(Side.CLIENT)
    private IIcon[] icons;

    @SideOnly(Side.CLIENT)
    public void registerIcons(IIconRegister par1IconRegister) {
        icons = new IIcon[subtypesAmount];
        for(int i = (subtypesAmount - 1); i >= 0; --i) {
            icons[i] = par1IconRegister.registerIcon(Names.genericUnlocalizedName + i);
        }
    }

    public IIcon getIconFromDamage(int i) {
        return icons[i];
    }

    public String getUnlocalizedName(ItemStack par1ItemStack) {
        int i = MathHelper.clamp_int(par1ItemStack.getItemDamage(), 0, 15);
        return "item." + Names.genericUnlocalizedName + ":" + names[i];
    }

    @SideOnly(Side.CLIENT)
    public void getSubItems(Item par1, CreativeTabs par2CreativeTabs, List par3List) {
        for (int x = 0; x < subtypesAmount; x++) {
            par3List.add(new ItemStack(this, 1, x));
        }
    }

    @Override
    public void addInformation(ItemStack stack, EntityPlayer player, List text, boolean whatDoesThisRandomBooleanDoQuestionMark)
    {
        final int i = stack.getItemDamage();
        switch(i) {
            case 0:
                break;
        }
        super.addInformation(stack, player, text, whatDoesThisRandomBooleanDoQuestionMark);
    }

    public boolean onItemUse(ItemStack stack, EntityPlayer p, World w, int x, int y, int z, int par7, float par8, float par9, float par10) {
        return true;
    }

}
