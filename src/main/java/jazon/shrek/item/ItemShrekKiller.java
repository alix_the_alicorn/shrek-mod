package jazon.shrek.item;

import jazon.shrek.CreativeTabsShrek;
import jazon.shrek.lib.Names;
import net.minecraft.item.ItemSword;
import net.minecraftforge.common.util.EnumHelper;

public class ItemShrekKiller extends ItemSword {
    public static final ToolMaterial shrekKiller = EnumHelper.addToolMaterial("shrekKiller", 100, 10, 100.0f, 9996.0f, 1000000);

    public ItemShrekKiller() {
        super(shrekKiller);
        setUnlocalizedName(Names.shrekKillerUnlocalizedName);
        setTextureName("wood_sword");
        setCreativeTab(CreativeTabsShrek.tabShrekCreative);
    }
}
