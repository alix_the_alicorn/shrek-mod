package jazon.shrek.proxy;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

public class ClientProxy extends CommonProxy {
    @Override
    public void clientPreInitialize(FMLPreInitializationEvent e) {
        for(int i = registers.length - 1; i >= 0; -- i) {
            registers[i].clientPreInitialize(e);
        }
    }

    @Override
    public void clientInitialize(FMLInitializationEvent e) {
        for(int i = registers.length - 1; i >= 0; -- i) {
            registers[i].clientInitialize(e);
        }
    }

    @Override
    public void clientPostInitialize(FMLPostInitializationEvent e) {
        for(int i = registers.length - 1; i >= 0; -- i) {
            registers[i].clientPostInitialize(e);
        }
    }
}
