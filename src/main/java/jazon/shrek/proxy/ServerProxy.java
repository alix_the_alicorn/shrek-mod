package jazon.shrek.proxy;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;

public class ServerProxy extends CommonProxy {
    @Override
    public void serverPreInitialize(FMLPreInitializationEvent e) {
        for(int i = registers.length - 1; i >= 0; -- i) {
            registers[i].serverPreInitialize(e);
        }
    }

    @Override
    public void serverInitialize(FMLInitializationEvent e) {
        for(int i = registers.length - 1; i >= 0; -- i) {
            registers[i].serverInitialize(e);
        }
    }

    @Override
    public void serverPostInitialize(FMLPostInitializationEvent e) {
        for(int i = registers.length - 1; i >= 0; -- i) {
            registers[i].serverPostInitialize(e);
        }
    }
}
