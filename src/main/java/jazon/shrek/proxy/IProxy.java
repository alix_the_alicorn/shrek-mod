package jazon.shrek.proxy;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;

public interface IProxy {
    public void commonPreInitialize(FMLPreInitializationEvent e);
    public void commonInitialize(FMLInitializationEvent e);
    public void commonPostInitialize(FMLPostInitializationEvent e);
    public void clientPreInitialize(FMLPreInitializationEvent e);
    public void clientInitialize(FMLInitializationEvent e);
    public void clientPostInitialize(FMLPostInitializationEvent e);
    public void serverPreInitialize(FMLPreInitializationEvent e);
    public void serverInitialize(FMLInitializationEvent e);
    public void serverPostInitialize(FMLPostInitializationEvent e);
}
