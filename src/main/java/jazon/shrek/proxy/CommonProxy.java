package jazon.shrek.proxy;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import jazon.shrek.proxy.register.*;

public class CommonProxy implements IProxy {
    //The "RegisterBase"s to run. Bottom first, top last.
    RegisterBase[] registers = {
            new RegisterRender(),
            new RegisterRecipe(),
            new RegisterOreDict(),
            new RegisterEntity(),
            new RegisterBlock(),
            new RegisterItem()
    };

    @Override
    public void commonPreInitialize(FMLPreInitializationEvent e) {
        for(int i = registers.length - 1; i >= 0; -- i) {
            registers[i].commonPreInitialize(e);
        }
    }

    @Override
    public void commonInitialize(FMLInitializationEvent e) {
        for(int i = registers.length - 1; i >= 0; -- i) {
            registers[i].commonInitialize(e);
        }
    }

    @Override
    public void commonPostInitialize(FMLPostInitializationEvent e) {
        for(int i = registers.length - 1; i >= 0; -- i) {
            registers[i].commonPostInitialize(e);
        }
    }

    @Override
    public void clientPreInitialize(FMLPreInitializationEvent e) {}
    @Override
    public void clientInitialize(FMLInitializationEvent e) {}
    @Override
    public void clientPostInitialize(FMLPostInitializationEvent e) {}
    @Override
    public void serverPreInitialize(FMLPreInitializationEvent e) {}
    @Override
    public void serverInitialize(FMLInitializationEvent e) {}
    @Override
    public void serverPostInitialize(FMLPostInitializationEvent e) {}
}
