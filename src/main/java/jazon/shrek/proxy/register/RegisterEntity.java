package jazon.shrek.proxy.register;

import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.EntityRegistry;
import jazon.shrek.Shrek;
import jazon.shrek.entity.living.EntityShrek;
import jazon.shrek.entity.living.EntityShrekDonkey;
import jazon.shrek.helper.Helper;
import net.minecraft.entity.EnumCreatureType;
import net.minecraft.world.biome.BiomeGenBase;

public class RegisterEntity extends RegisterBase {
    private void registerEntities() {
        int id = 0;
        EntityRegistry.registerModEntity(EntityShrek.class, "shrek", id, Shrek.instance, 64, 1, true);
        ++id;
        EntityRegistry.registerModEntity(EntityShrekDonkey.class, "donkey", id, Shrek.instance, 64, 1, true);
        ++id;
    }

    private void registerEntitySpawns() {
        BiomeGenBase[] shrekBiomes = {BiomeGenBase.swampland};
        EntityRegistry.addSpawn(EntityShrek.class, 20, 1, 1, EnumCreatureType.creature, shrekBiomes);
    }

    @Override
    public void commonPreInitialize(FMLPreInitializationEvent e) {
        registerEntities();
        registerEntitySpawns();
    }
}
