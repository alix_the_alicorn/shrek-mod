package jazon.shrek.proxy.register;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import jazon.shrek.proxy.IProxy;

public class RegisterBase implements IProxy {
    @Override
    public void commonPreInitialize(FMLPreInitializationEvent e) {

    }

    @Override
    public void commonInitialize(FMLInitializationEvent e) {

    }

    @Override
    public void commonPostInitialize(FMLPostInitializationEvent e) {

    }

    @Override
    public void clientPreInitialize(FMLPreInitializationEvent e) {

    }

    @Override
    public void clientInitialize(FMLInitializationEvent e) {

    }

    @Override
    public void clientPostInitialize(FMLPostInitializationEvent e) {

    }

    @Override
    public void serverPreInitialize(FMLPreInitializationEvent e) {

    }

    @Override
    public void serverInitialize(FMLInitializationEvent e) {

    }

    @Override
    public void serverPostInitialize(FMLPostInitializationEvent e) {

    }
}
