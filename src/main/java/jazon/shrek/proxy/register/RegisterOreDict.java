package jazon.shrek.proxy.register;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import jazon.shrek.lib.OreDictNames;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;

public class RegisterOreDict extends RegisterBase {
    @Override
    public void commonInitialize(FMLInitializationEvent e) {
        OreDictionary.registerOre(OreDictNames.cropOnion, new ItemStack(RegisterItem.onion, 1, OreDictionary.WILDCARD_VALUE));
    }
}