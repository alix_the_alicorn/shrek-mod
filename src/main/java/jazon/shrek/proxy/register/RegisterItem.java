package jazon.shrek.proxy.register;

import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import jazon.shrek.item.*;
import jazon.shrek.lib.Names;
import net.minecraft.item.Item;

public class RegisterItem extends RegisterBase {
    public static Item spawnEgg = new ItemSpawnEgg();
    public static Item onion = new ItemOnion();
    public static Item onionOnAStick = new ItemOnionOnAStick();
    public static Item shrekKiller = new ItemShrekKiller();
    public static Item parfait = new ItemParfait();
    public static Item generic = new ItemGeneric();

    @Override
    public void commonPreInitialize(FMLPreInitializationEvent e) {
        GameRegistry.registerItem(spawnEgg, Names.spawnEggName);
        GameRegistry.registerItem(onion, Names.onionName);
        GameRegistry.registerItem(onionOnAStick, Names.onionOnAStickName);
        GameRegistry.registerItem(shrekKiller, Names.shrekKillerName);
        GameRegistry.registerItem(parfait, Names.parfaitName);
        GameRegistry.registerItem(generic, Names.genericName);
    }
}
