package jazon.shrek.proxy.register;

import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import jazon.shrek.block.BlockOnionPlant;
import jazon.shrek.lib.Names;
import net.minecraft.block.Block;

public class RegisterBlock extends RegisterBase {
    public static Block onionPlant = new BlockOnionPlant();;

    @Override
    public void commonPreInitialize(FMLPreInitializationEvent e) {
        GameRegistry.registerBlock(onionPlant, Names.onionPlantName);
    }
}