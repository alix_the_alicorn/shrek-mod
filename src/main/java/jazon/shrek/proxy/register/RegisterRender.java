package jazon.shrek.proxy.register;

import cpw.mods.fml.client.registry.RenderingRegistry;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import jazon.shrek.client.model.entity.living.ModelEntityShrekDonkey;
import jazon.shrek.client.render.entity.living.RenderEntityShrek;
import jazon.shrek.client.render.entity.living.RenderEntityShrekDonkey;
import jazon.shrek.entity.living.EntityShrek;
import jazon.shrek.entity.living.EntityShrekDonkey;

public class RegisterRender extends RegisterBase {
    @Override
    public void clientInitialize(FMLInitializationEvent e) {
        RenderingRegistry.registerEntityRenderingHandler(EntityShrek.class, new RenderEntityShrek());
        RenderingRegistry.registerEntityRenderingHandler(EntityShrekDonkey.class, new RenderEntityShrekDonkey(new ModelEntityShrekDonkey(), new ModelEntityShrekDonkey(), 0.7F));
    }
}
