package jazon.shrek.proxy.register;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.registry.GameRegistry;
import jazon.shrek.lib.OreDictNames;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;

public class RegisterRecipe extends RegisterBase {
    @Override
    public void commonInitialize(FMLInitializationEvent e) {
        GameRegistry.addRecipe(new ShapedOreRecipe(RegisterItem.onionOnAStick, true, new Object[]{"RO", Character.valueOf('R'), new ItemStack(Items.fishing_rod, 1, OreDictionary.WILDCARD_VALUE), Character.valueOf('O'), OreDictNames.cropOnion}));
        GameRegistry.addRecipe(new ItemStack(RegisterItem.generic, 1, 0), "g g", "ggg", " g ", 'g', new ItemStack(Blocks.glass));
        GameRegistry.addShapelessRecipe(new ItemStack(RegisterItem.parfait, 1, 0), new ItemStack(Items.milk_bucket, 1, 0), new ItemStack(RegisterItem.generic, 1, 0), new ItemStack(Blocks.ice, 1, 0), new ItemStack(Items.melon, 1, 0));
    }
}