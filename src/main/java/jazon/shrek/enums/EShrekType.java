package jazon.shrek.enums;

public enum EShrekType {
    IMPOSTER((byte)0, "imposter"),
    NORMAL((byte)1, "normal"),
    KNIGHT((byte)2, "knight"),
    PIRATE((byte)3, "pirate"),
    REGAL((byte)4, "regal"),
    SWIM((byte)5, "swim");

    public final byte id;
    public final String folder;

    EShrekType(byte idv, String folderv) {
        id = idv;
        folder = folderv;
    }

    public byte getId() {
        return id;
    }

    public static EShrekType getShrekType(byte id) {
        switch(id) {
            case 0:
                return IMPOSTER;
            case 1:
                return NORMAL;
            case 2:
                return KNIGHT;
            case 3:
                return PIRATE;
            case 4:
                return REGAL;
            case 5:
                return SWIM;
            default:
                return NORMAL;
        }
    }

    public String getFolder() {
        return folder;
    }
}